const express = require('express');
const redis = require('redis');
const app = express();
const port = 3000;



const client = redis.createClient({ host: "redis" })

app.get('/', function (req, res) {
  res.send("Hi There");
});

app.get('/visite', function (req, res) {
  client.get("visite", (err, reply) => {
    res.json({ "visite": parseInt(reply) })
  })

});
app.post('/update', function (req, res) {

  let visite = JSON.parse(req.body.visite)
  client.set("visite", visite)

});


app.listen(port, function () {
  client.get("visite", function (err, reply) {
    if (err) { client.set("visite", "0"); }
  });
  console.log("Port 3000!")
})