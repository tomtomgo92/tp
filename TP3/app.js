const express = require('express');
const redis = require('pg');
const fs = require('fs')
const app = express();
const port = 3000;



const client = new redis.Client({ host: process.env.POSTGRES_HOST, database: process.env.POSTGRES_DB, user: fs.readFileSync(process.env.POSTGRES_USER_FILE).toString(), password: fs.readFileSync(process.env.POSTGRES_PASSWORD_FILE).toString() })

client.connect().then(() => { console.log('coucou') }).catch(err => { console.log(err) })
app.get('/', function (req, res) {
  res.send("Hi There");
});


app.listen(port, function () {
  console.log("Port 3000!")
})